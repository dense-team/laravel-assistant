<?php
/**
 * User: Maros Jasan
 * Date: 31.1.2018
 * Time: 17:32
 */

use PHPUnit\Framework\TestCase;

use Dense\Assistant\View\Directive;

class BladeDirectivesTest extends TestCase
{
    public function testDirectiveOpenWindowed()
    {
        $expected = 'onclick="window.open(this.href); return false;"';
        $result = Directive::openWindowed();

        $this->assertEquals($expected, $result);
    }

    public function testDirectiveConfirmDelete()
    {
        $message = 'Really delete?';

        $expected = 'onclick="if(!confirm(\''. $message .'\')) { return false; }"';
        $result = Directive::confirmDelete($message);

        $this->assertEquals($expected, $result);
    }
}
