# Various helpers for Lavarel by Dense

## Middlewares

### Asset Versioning

Add following code to app//Http/Kernel.php file
```
\Dense\Assistant\Middleware\AssetVersioning::class
```
### Json Encodings

Add following code to app//Http/Kernel.php file
```
\Dense\Assistant\Middleware\JsonEncodings::class
```
