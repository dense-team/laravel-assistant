<?php
/**
 * User: Maros Jasan
 * Date: 11.11.2019
 * Time: 11:14 PM
 */

namespace Dense\Assistant\View;

class Directive
{
    /**
     * @return string
     */
    public static function openWindowed()
    {
        return 'onclick="window.open(this.href); return false;"';
    }

    /**
     * @param string $message
     * @return string
     */
    public static function confirmDelete($message)
    {
        return 'onclick="if(!confirm(\''. $message .'\')) { return false; }"';
    }
}
