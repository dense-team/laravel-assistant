<?php
/**
 * User: Maros Jasan
 * Date: 7.3.2017
 * Time: 18:44
 */

namespace Dense\Assistant\Middleware;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;

class AssetVersioning
{
    public function handle($request, \Closure $next)
    {
        $versioning = time();
        if (App::environment(['prod', 'stage'])) {
            $versioning = date('Ymd');
        }

        View::share([
            'versioning' => $versioning,
        ]);

        return $next($request);
    }
}
