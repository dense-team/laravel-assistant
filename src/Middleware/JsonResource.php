<?php
/**
 * User: Maros Jasan
 * Date: 7.3.2017
 * Time: 18:44
 */

namespace Dense\Assistant\Middleware;

class JsonResource
{
    public function handle($request, \Closure $next)
    {
        $request->headers->set('Accept', 'application/json');

        $response = $next($request);

        $response->setEncodingOptions(JSON_NUMERIC_CHECK);

        return $response;
    }
}
