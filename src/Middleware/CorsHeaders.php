<?php

namespace App\Http\Middleware;

class CorsHeaders
{
    public function handle($request, \Closure $next)
    {
        return $next($request)
            ->header('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, DELETE')
            ->header('Access-Control-Allow-Headers', 'Authorization')
            ->header('Access-Control-Allow-Origin', '*');
    }
}
