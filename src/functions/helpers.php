<?php

use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;

if (!function_exists('os')) {

    function os()
    {
        if (str_contains(strtolower(PHP_OS), 'linux')) {
            return 'linux';
        } elseif (str_contains(strtolower(PHP_OS), 'win')) {
            return 'windows';
        } else {
            throw new \Exception('Unsupported operating system.');
        }
    }
}

if (!function_exists('arrayize')) {

    function arrayize($item)
    {
        if (is_scalar($item)) {
            $item = [$item];
        }

        return $item;
    }
}

if (!function_exists('collectize')) {

    function collectize($value)
    {
        if ($value instanceof Collection) {
            return $value;
        }

        if (!is_array($value)) {
            $value = [$value];
        }

        return new Collection($value);
    }
}

if (!function_exists('request_path')) {

    function request_path(array $params = [], $hash = null)
    {
        $query = null;

        if (!empty($params)) {
            $query .= '?' . http_build_query($params);
        }

        if (!is_null($hash)) {
            $query .= '#' . $hash;
        }

        return '/' . trim(Request::path(), '/') . $query;
    }
}

if (!function_exists('current_route')) {

    function current_route()
    {
        return Route::currentRouteName();
    }
}

if (!function_exists('get_descr')) {

    function get_descr($text, $length = 200)
    {
        $descr = html_entity_decode(trim(strip_tags($text)));

        if ($length < strlen($descr)) {
            if (!($offset = strpos($descr, ' ', $length))) {
                $offset = $length;
            }

            $descr = substr($descr, 0, $offset);
        }

        return $descr;
    }
}

if (!function_exists('get_number')) {

    function get_number($number, $decimals = 2)
    {
        return number_format((float)$number, $decimals, ',', ' ');
    }
}

if (!function_exists('get_percent')) {

    function get_percent($number)
    {
        return get_number($number, 0) . '%';
    }
}

if (!function_exists('get_price')) {

    function get_price($number)
    {
        return get_number($number, 2) . '€';
    }
}

if (!function_exists('get_datetime')) {

    function get_datetime($datetime = 'now', $showSeconds = true)
    {
        $format = 'd.m.Y H:i:s';
        if ($showSeconds !== true) {
            $format = 'd.m.Y H:i';
        }

        return $datetime ? date($format, strtotime($datetime)) : null;
    }
}

if (!function_exists('get_date')) {

    function get_date($date = 'today')
    {
        return $date ? date('d.m.Y', strtotime($date)) : null;
    }
}

if (!function_exists('get_time')) {

    function get_time($datetime = 'now', $showSeconds = true)
    {
        $format = 'H:i:s';
        if ($showSeconds !== true) {
            $format = 'H:i';
        }

        return $datetime ? date($format, strtotime($datetime)) : null;
    }
}

if (!function_exists('get_weekday')) {

    function get_weekday($date = 'today')
    {
        return $date ? ucfirst(strftime('%A', strtotime($date))) : null;
    }
}


if (!function_exists('html_date')) {

    function html_date($date = 'today')
    {
        $format = 'Y-m-d';

        return $date ? date($format, strtotime($date)) : null;
    }
}

if (!function_exists('html_datetime')) {

    function html_datetime($datetime = 'now')
    {
        $format = 'Y-m-d\TH:i';

        return $datetime ? date($format, strtotime($datetime)) : null;
    }
}

if (!function_exists('cookie_name')) {

    function cookie_name($name)
    {
        return Str::slug(config('app.name')) . '_' . $name;
    }
}

if (!function_exists('random_color')) {

    function random_color()
    {
        $part = function () {
            return str_pad(dechex(mt_rand(0, 255)), 2, '0', STR_PAD_LEFT);
        };

        return '#' . $part() . $part() . $part();
    }
}
