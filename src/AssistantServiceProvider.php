<?php

namespace Dense\Assistant;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

use Dense\Assistant\View\Directive;

class AssistantServiceProvider extends ServiceProvider
{
    /**
     * @var string
     */
    protected $namespace = 'assistant';

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // laravel langs
        $this->loadJsonTranslationsFrom(__DIR__ . '/resources/laravel/lang');

        // assistant langs
        $this->loadTranslationsFrom(__DIR__ . '/resources/lang', $this->namespace);
        $this->loadJsonTranslationsFrom(__DIR__ . '/resources/lang');

        $this->publishes([
            __DIR__ . '/resources/lang' => resource_path('lang/assistant'),
        ]);

        // views
        $this->loadViewsFrom(__DIR__ . '/resources/views', $this->namespace);

        $this->publishes([
            __DIR__ . '/views' => resource_path('views/assistant'),
        ]);

        Blade::directive('openWindowed', function () {
            return Directive::openWindowed();
        });

        Blade::directive('confirmDelete', function () {
            $message = __('Really delete?');

            return Directive::confirmDelete($message);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
