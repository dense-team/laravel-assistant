<?php

return [
    'read' => 'Čítanie',
    'write' => 'Zápis',
    'delete' => 'Mazanie',
    'none' => 'Zakázaný prístup',
    'full' => 'Plný prístup',
    'inherit' => 'Zdedený prístup',
];
