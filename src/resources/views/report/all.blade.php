<?php
    if(!isset($prefix)) {
        $prefix = null;
    } else {
        $prefix = $prefix . '-';
    }
?>

@if($fail = Session::get($prefix . 'fail'))
    @include('assistant::report.simple', [
        'reportMessage' => $fail,
        'reportType' => 'danger',
    ])
@endif

@if($success = Session::get($prefix . 'success'))
    @include('assistant::report.simple', [
        'reportMessage' => $success,
        'reportType' => 'success',
    ])
@endif

@if($warning = Session::get($prefix . 'warning'))
    @include('assistant::report.simple', [
        'reportMessage' => $warning,
        'reportType' => 'warning',
    ])
@endif

@if($info = Session::get($prefix . 'info'))
    @include('assistant::report.simple', [
        'reportMessage' => $info,
        'reportType' => 'info',
    ])
@endif
