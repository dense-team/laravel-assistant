<div class="alert alert-{{ $reportType ?? 'danger' }}" role="alert">
    {{ $reportMessage }}
</div>
