@if ($errors->has($field))
    <div class="has-error">
        <span class="help-block">{{ $errors->first($field) }}</span>
    </div>
@endif
